import pytest
import requests
import time
import sys
import config


@pytest.fixture(scope="module")
def connection(request):
    address = getattr(request.module, "connection_address", config.allBreaches)
    time.sleep(config.delay)
    return requests.get(address, headers=config.headers)


connection_address = config.allBreaches
@pytest.mark.apitest
def test_pwned_Api2(connection):
    assert connection.status_code == 200

connection_address = config.allBreaches
def test_count_all_breaches2(connection):
    resp_json = connection.json()
    # print(len(resp_json))
    assert len(resp_json) >= config.branchCount

@pytest.mark.xfail(reason='Error with asynchronous request')
# connection_address = mockEmail
def test_pwnded_breaches2(connection):
    resp_json = connection.json()
    breaches = resp_json['Breaches']
    assert len(breaches) > 0

connection_address = config.mockDomain
def test_domain_existing(connection):
    resp_json = connection.json()
    # if domain was pwned, then array resp_json has length greater than 0
    assert len(resp_json) > 0

@pytest.mark.skip(reason='test is not ready yet')
# connection_address = mockDomainNotExisting
def test_domain_not_existing(connection):
    resp_json = connection.json()
    # if domain not existing, array resp_json is empty
    assert len(resp_json) == 0

connection_address = config.branchesName
def test_check_branches_by_name(connection):
    resp_json = connection.json()
    assert len(resp_json) > 0

@pytest.mark.skipif(sys.platform =='win32', reason='Linux test')
# connection_address = branchesNotExistingName
def test_check_branches_by_not_existing_name(connection):
    assert connection.status_code == 404


connection_address = config.allBreaches
def test_columns_name2(connection):
    resp_json = connection.json()
    for breach in resp_json:
        assert check_branch_keys(breach) == True


def check_branch_keys(breach):
    keys = ['Name', 'Domain', 'PwnCount', 'BreachDate', 'DataClasses']
    for key in keys:
        # check if key exist in object
        try:
            breach[key]
        except NameError:
            return False
        # check the value in object for current key
        # if not breach[key]:
        #     return key
    return True
