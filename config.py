headers = {'User-Agent': 'pytest-api-study-wsb', 'From': 'kiszka.joanna@wp.pl'}
mockEmail = 'https://haveibeenpwned.com/api/v2/unifiedsearch/test%40test.pl'
allBreaches = 'https://haveibeenpwned.com/api/v2/breaches'
mockDomain = 'https://haveibeenpwned.com/api/v2/breaches?domain=adobe.com'
mockDomainNotExisting = 'https://haveibeenpwned.com/api/v2/breaches?domain=mydomain.pl'
branchesName = 'https://haveibeenpwned.com/api/v2/breach/BeautifulPeople'
branchesNotExistingName = 'https://haveibeenpwned.com/api/v2/breach/BlaBla'
branchCount = 285
delay = 1.5
