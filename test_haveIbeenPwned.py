
from data.connection import Connection
import requests
import time
import pytest
import config

connection = Connection()
is_API_correct = True

# use of the Connection class
def test_API():
    global is_API_correct
    is_API_correct = False
    assert connection.get(config.allBreaches).status_code == 200

@pytest.mark.skipif(is_API_correct == True, reason="set False in API test")
def test_API2():
    assert len(connection.get(allBreaches).json()) >= config.branchCount


# use of the parametrization
@pytest.mark.parametrize("test_input, expected", [
    (len(connection.get(config.allBreaches).json()), config.branchCount)
])
def test_counter(test_input, expected):
    assert test_input == expected


# check request status is 200
def test_pwned_Api():
    resp = requests.get(config.allBreaches, headers=config.headers)
    assert resp.status_code == 200


# list all of breaches
def test_count_all_breaches():
    time.sleep(config.delay)  # API blocked requests more frequent than 1500 milliseconds
    resp = requests.get(config.allBreaches, headers=config.headers)
    resp_json = resp.json()
    # print(len(resp_json))
    assert len(resp_json) >= config.branchCount


# List of all breaches for an account - email found in the list of breaches
def test_pwnded_breaches():
    time.sleep(config.delay)  # API blocked requests more frequent than 1500 milliseconds
    resp = requests.get(config.mockEmail, headers=config.headers)
    resp_json = resp.json()
    breaches = resp_json['Breaches']
    # print(breaches)
    assert len(breaches) > 0


# Check breaches by domain - for domain existing in list of all breaches
def test_domain_existing():
    time.sleep(config.delay)
    resp = requests.get(config.mockDomain, headers=config.headers)
    resp_json = resp.json()
    # if domain was pwned, then array resp_json has length greater than 0
    assert len(resp_json) > 0


# Check breaches by domain - for domain not existing in list of all breaches
def test_domain_not_existing():
    time.sleep(config.delay)
    resp = requests.get(config.mockDomainNotExisting, headers=config.headers)
    resp_json = resp.json()
    # if domain not existing, array resp_json is empty
    assert len(resp_json) == 0


# Check breaches by name - for name existing in list of all breaches
def test_check_branches_by_name():
    time.sleep(config.delay)
    resp = requests.get(config.branchesName, headers=config.headers)
    resp_json = resp.json()
    assert len(resp_json) > 0


# Check breaches by name - for name not existing in list of all breaches
def test_check_branches_by_not_existing_name():
    time.sleep(config.delay)
    resp = requests.get(config.branchesNotExistingName, headers=config.headers)
    assert resp.status_code == 404

# Check columns names
def test_columns_name():
    time.sleep(config.delay)  # API blocked requests more frequent than 1500 milliseconds
    resp = requests.get(config.allBreaches, headers=config.headers)
    resp_json = resp.json()
    for breach in resp_json:
        assert check_branch_keys(breach) == True


def check_branch_keys(breach):
    keys = ['Name', 'Domain', 'PwnCount', 'BreachDate', 'DataClasses']
    for key in keys:
        # check if key exist in object
        try:
            breach[key]
        except NameError:
            return False
        # check the value in object for current key
        # if not breach[key]:
        #     return key
    return True



