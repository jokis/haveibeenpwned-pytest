import requests
import time

class Connection:
    def __init__(self):
        self.headers = {
                        'User-Agent': 'pytest-api-study-wsb',
                        'From': 'kiszka.joanna@wp.pl'
                        }

    def get(self, address, delay = 1.5):
        time.sleep(delay)
        return requests.get(address, headers=self.headers)


